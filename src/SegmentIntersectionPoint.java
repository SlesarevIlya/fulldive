public class SegmentIntersectionPoint {

    boolean existence;
    double X = 0.0;
    double Y = 0.0;

    SegmentIntersectionPoint(boolean existence) {
        this.existence = existence;
    }

    SegmentIntersectionPoint(boolean existence, double X, double Y) {
        this.existence = existence;
        this.X = X;
        this.Y = Y;
    }

    @Override
    public String toString() {
        if (this.existence) {
            return "X = " + X + "; Y = " + Y;
        }

        return "Not segment intersection";
    }
}