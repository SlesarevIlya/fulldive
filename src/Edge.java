public class Edge implements Comparable<Edge>{
    double beginX;
    double beginY;
    double endX;
    double endY;
    boolean projection = false;

    Edge(double x1, double y1, double x2, double y2) {
        if (x1 > x2) {
            this.beginX = x2;
            this.beginY = y2;
            this.endX = x1;
            this.endY = y1;
        } else if (x2 > x1) {
            this.beginX = x1;
            this.beginY = y1;
            this.endX = x2;
            this.endY = y2;
        } else {
            if (y1 > y2) {
                this.beginX = x1;
                this.beginY = y1;
                this.endX = x2;
                this.endY = y2;
            } else {
                this.beginX = x2;
                this.beginY = y2;
                this.endX = x1;
                this.endY = y1;
            }
        }
    }

    @Override
    public int compareTo(Edge other) {
        if (this.beginX > other.beginX) {
            return 1;
        } else if (this.beginX == other.beginX) {
            return 0;
        } else {
            return -1;
        }
    }
}