import java.util.ArrayList;
import java.util.Collections;

class FirstTask {

    private double cameraX;
    private double cameraY;
    private double E;

    FirstTask(double cameraX, double cameraY, double E) {
        this.cameraX = cameraX;
        this.cameraY = cameraY;
        this.E = E;
    }

    boolean isVisible(ArrayList<Edge> edges) {
        Edge redEdge = edges.get(edges.size() - 1);
        Edge firstVector = new Edge(0, 0, redEdge.beginX, redEdge.beginY);
        Edge secondVector = new Edge(0, 0, redEdge.endX, redEdge.endY);
        edges.remove(edges.size() - 1);

        for (Edge edge : edges) {

            //work with segments
            {
                SegmentIntersectionPoint intersectionPoint1 = getSegmentIntersection(edge, firstVector);
                SegmentIntersectionPoint intersectionPoint2 = getSegmentIntersection(edge, secondVector);
                SegmentIntersectionPoint intersectionWithRedEdge = getSegmentIntersection(edge, redEdge);

                if (intersectionPoint1.existence && intersectionPoint2.existence) {
                    if (intersectionPoint1.X < intersectionPoint2.X) {
                        edge.beginX = intersectionPoint1.X;
                        edge.beginY = intersectionPoint1.Y;
                        edge.endX = intersectionPoint2.X;
                        edge.endY = intersectionPoint2.Y;
                    } else {
                        edge.beginX = intersectionPoint2.X;
                        edge.beginY = intersectionPoint2.Y;
                        edge.endX = intersectionPoint1.X;
                        edge.endY = intersectionPoint1.Y;
                    }
                } else if (intersectionWithRedEdge.existence) {
                    if (intersectionPoint1.existence) {
                        edge.beginX = intersectionPoint1.X;
                        edge.beginY = intersectionPoint1.Y;
                        edge.endX = intersectionWithRedEdge.X;
                        edge.endY = intersectionWithRedEdge.Y;
                    } else if (intersectionPoint2.existence) {
                        edge.beginX = intersectionWithRedEdge.X;
                        edge.beginY = intersectionWithRedEdge.Y;
                        edge.endX = intersectionPoint2.X;
                        edge.endY = intersectionPoint2.Y;
                    } else if (isPointInTriangle(redEdge, edge.beginX, edge.beginY)) {
                        edge.endX = intersectionWithRedEdge.X;
                        edge.endY = intersectionWithRedEdge.Y;
                    } else {
                        edge.beginX = intersectionWithRedEdge.X;
                        edge.beginY = intersectionWithRedEdge.Y;
                    }
                } else if (intersectionPoint1.existence) {
                    if (isPointInTriangle(redEdge, edge.beginX, edge.beginY)) {
                        edge.endX = intersectionPoint1.X;
                        edge.endY = intersectionPoint1.Y;
                    } else {
                        edge.beginX = intersectionPoint1.X;
                        edge.beginY = intersectionPoint1.Y;
                    }
                } else if (intersectionPoint2.existence) {
                    if (isPointInTriangle(redEdge, edge.beginX, edge.beginY)) {
                        edge.endX = intersectionPoint2.X;
                        edge.endY = intersectionPoint2.Y;
                    } else {
                        edge.beginX = intersectionPoint2.X;
                        edge.beginY = intersectionPoint2.Y;
                    }
                }
            }

            //work with straights
            {
                SegmentIntersectionPoint intersectionPoint1 = getSegmentIntersection(redEdge,
                        new Edge(cameraX, cameraY, edge.beginX, edge.beginY));
                SegmentIntersectionPoint intersectionPoint2 = getSegmentIntersection(redEdge,
                        new Edge(cameraX, cameraY, edge.endX, edge.endY));

                if (isPointInTriangle(redEdge, edge.beginX, edge.beginY)) {
                    edge.beginX = intersectionPoint1.X;
                    edge.beginY = intersectionPoint1.Y;
                    edge.projection = true;
                }
                if (isPointInTriangle(redEdge, edge.endX, edge.endY)) {
                    edge.endX = intersectionPoint2.X;
                    edge.endY = intersectionPoint2.Y;
                    edge.projection = true;
                }
            }

        }

        for (Edge edge : edges) {
            if (edge.beginX > edge.endX) {
                double tmpX = edge.beginX;
                double tmpY = edge.beginY;
                edge.beginX = edge.endX;
                edge.beginY = edge.endY;
                edge.endX = tmpX;
                edge.endY = tmpY;
            }
        }
        Collections.sort(edges);

        double cursorX = 0.0;
        for (Edge edge : edges) {
            if (!edge.projection) {
                continue;
            }

            if (edge.beginX - redEdge.beginX > E) {
                return true;
            }

            cursorX = edge.endX;
            break;
        }

        for (Edge edge : edges) {
            if (edge.projection) {
                if ((edge.beginX <= cursorX) || (Math.abs(edge.beginX - cursorX)) < E) {
                    if (edge.endX > cursorX) {
                        cursorX = edge.endX;
                    }
                } else {
                    return true;
                }
            }
        }

        return Math.abs(cursorX - redEdge.endX) > E;
    }

    private SegmentIntersectionPoint getSegmentIntersection(Edge edge1, Edge edge2) {

        if ((edge1.beginX - edge1.endX == 0) && (edge2.beginX - edge2.endX == 0)) {
            return new SegmentIntersectionPoint(false);
        }

        // y = a1 * x + b1
        // y = a2 * x + b2
        // a == tg

        if (edge1.beginX - edge1.endX == 0) {
            return checkVertical(edge1, edge2);
        } else if (edge2.beginX - edge2.endX == 0) {
            return checkVertical(edge2, edge1);
        }

        double a1 = (edge1.beginY - edge1.endY) / (edge1.beginX - edge1.endX);
        double a2 = (edge2.beginY - edge2.endY) / (edge2.beginX - edge2.endX);
        double b1 = edge1.beginY - a1 * edge1.beginX;
        double b2 = edge2.beginY - a2 * edge2.beginX;

        if (a1 == a2) {
            return new SegmentIntersectionPoint(false);
        }

        double X = (b2 - b1) / (a1 - a2);
        double Y = a1 * X + b1;

        if ((X < Math.max(edge1.beginX, edge2.beginX)) ||
                (X > Math.min(edge1.endX, edge2.endX))) {

            return new SegmentIntersectionPoint(false, X, Y);
        } else {
            return new SegmentIntersectionPoint(true, X, Y);
        }
    }

    private SegmentIntersectionPoint checkVertical(Edge edge1, Edge edge2) {
        double X = edge1.beginX;
        double a2 = (edge2.beginY - edge2.endY) / (edge2.beginX - edge2.endX);
        double b2 = edge2.beginY - a2 * edge2.beginX;
        double Y = a2 * X + b2;

        if (edge2.beginX <= X && edge2.endX >= X &&
                Math.min(edge1.beginY, edge1.endY) <= Y &&
                Math.max(edge1.beginY, edge1.endY) >= Y) {

            return new SegmentIntersectionPoint(true, X, Y);
        }

        return new SegmentIntersectionPoint(false, X, Y);
    }

    private boolean isPointInTriangle(Edge redEdge, double pointX, double pointY) {
        double S = square(cameraX, cameraY, redEdge.beginX, redEdge.beginY, pointX, pointY)
                + square(redEdge.beginX, redEdge.beginY, redEdge.endX, redEdge.endY, pointX, pointY) +
                square(redEdge.endX, redEdge.endY, cameraX, cameraY, pointX, pointY);
        return Double.isNaN(S) || Math.abs(square(cameraX, cameraY, redEdge.beginX, redEdge.beginY, redEdge.endX, redEdge.endY) - S) < 0.001;
    }

    private double square(double x1, double y1, double x2, double y2, double x3, double y3) {
        double a = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
        double b = Math.sqrt((x2 - x3) * (x2 - x3) + (y2 - y3) * (y2 - y3));
        double c = Math.sqrt((x3 - x1) * (x3 - x1) + (y3 - y1) * (y3 - y1));
        double p = (a + b + c) / 2;

        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }

}
