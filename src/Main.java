import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {

        ArrayList<Edge> edges = new ArrayList<>();
        List<String> lines = Files.readAllLines(Paths.get("input.txt"), StandardCharsets.UTF_8);
        for (String line: lines){
            String[] lineSplit = line.split(" ");
            edges.add(new Edge(Double.parseDouble(lineSplit[0]), Double.parseDouble(lineSplit[1]),
                    Double.parseDouble(lineSplit[2]), Double.parseDouble(lineSplit[3])));
        }

        FirstTask ft = new FirstTask(0.0, 0.0, 0.0001);
        if (ft.isVisible(edges)) {
            System.out.println("The red segment is visible");
        } else {
            System.out.println("The red segment is not visible");
        }
    }

}
